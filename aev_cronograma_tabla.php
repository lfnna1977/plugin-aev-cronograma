<?php
/**
 * @package AEV_Cronograma_Tabla
 */
?>
<div class="table-responsive">
  <table class="table table-bordered">
    <thead>
      <tr>
        <th rowspan="2">#</th>
        <th rowspan="2">Nombre y Apellidos</th>
        <th rowspan="2">Cargo</th>
        <th rowspan="2">Nombre del Proyecto</th>
        <th rowspan="2">Objetivo del Viaje</th>
        <th rowspan="2">Destino</th>
        <th colspan="2">Salida</th>
        <th colspan="2">Retorno</th>
        <th rowspan="2">Vehiculo</th>        
      </tr>
      <tr>      
        <th>Fecha</th>
        <th>Hora</th>
        <th>Fecha</th>
        <th>Hora</th>
      </tr>
    </thead>
    <tbody>
      {% set counter = 1 %}
      {% for viaje in viajes %}      
      <tr>
        <td>{{ counter }}</td>
        <td>{{ viaje.nombreCompleto }}</td>
        <td>{{ viaje.cargo }}</td>
        <td>{{ viaje.nombreProyecto|convert_encoding('UTF-8', 'ISO-8859-1') }}</td>
        <td>{{ viaje.objetivoViaje }}</td>
        <td>{{ viaje.destino|convert_encoding('UTF-8', 'ISO-8859-1') }}</td>
        <td>{{ viaje.fechaSalida }}</td>
        <td>{{ viaje.horaSalida }}</td>
        <td>{{ viaje.fechaRetorno }}</td>
        <td>{{ viaje.horaRetorno }}</td>
        <td>{{ viaje.vehiculo }}</td>
      </tr>
        {% set counter = counter + 1 %}      
      {% endfor %}    
    </tbody>
  </table>
</div>

<!-- Single button -->
<div class="btn-group" role="group" aria-label="...">
  {% for keyMesAnio, mesAnio in mesesAnio %}
    {% if mesAnio == mesLiteral %}    
  <button type="button" class="btn btn-primary">{{ mesAnio }}</button>
    {% else %}
  <button type="button" class="btn btn-default" onclick="$(location).attr('href','menu.php?mes={{ keyMesAnio }}');">{{ mesAnio }}</button>
    {% endif %}
  {% endfor %}
</div>

<div class="table-responsive">
  <table class="table table-bordered">
    <caption>
      Elija una fecha para introducir el viaje
    </caption>
    <thead>      
      <tr>
        <th>#</th>
        <th>LUNES</th>
        <th>MARTES</th>
        <th>MIERCOLES</th>
        <th>JUEVES</th>
        <th>VIERNES</th>
        <th>SABADO</th>
        <th>DOMINGO</th>
      </tr>    
    </thead>
    <tbody>
      {% set contador = 1 %}
      {% for calendario in calendarios %}
      <tr>
        <td>{{ contador }}</td>
        <td>
        {% if calendario.0 is not null %}
          <div class="panel panel-default">
            <div class="panel-heading">{{ calendario.0 }} <button type="button" class="btn btn-success btn-xs" onclick="$(location).attr('href','viaje.insertar.php?fecha=2015-{{ mes }}-{% if calendario.0 < 10 %}0{{ calendario.0 }}{% else %}{{ calendario.0 }}{% endif %}');">+</button></div>            
            <table class="table .table-condensed">
              <thead>
                <tr>
                  <th>Proyecto</th>
                  <th>Técnico</th>
                  <th>Hora</th>
                  <th>Motivo</th>
                  <th>Vehiculo</th>
                  <th>Acción</th>
                </tr>
              </thead>
              {% if fiscales[calendario.0] is not null %}
              <tbody>
                {% for fiscal in fiscales[calendario.0] %}
                <tr bgcolor="{{ fiscal.color }}">
                  {% set proyectos = fiscal.nombreProyecto|split(',') %}                  
                  <td>{% for proyecto in proyectos %}[{{ proyecto|convert_encoding('UTF-8', 'ISO-8859-1') }}]<br />{% endfor %}</td>
                  <td>{{ fiscal.alias }}</td>                  
                  <td>{{ fiscal.hora }}</td>
                  <td>{{ fiscal.motivo }}</td> 
                  <td>{{ fiscal.vehiculo }}</td>
                  <td><a href="viaje.eliminar.php?id={{ fiscal.id }}" class="btn btn-danger btn-xs popconfirm">-</a> <button type="button" class="btn btn-primary btn-xs" onclick="$(location).attr('href','viaje.editar.php?id={{ fiscal.id }}');">*</button></td>
                </tr>
                {% endfor %}
              </tbody>
              {% endif %}
            </table>
          </div>
        {% endif %}
        </td>
        <td>
        {% if calendario.1 is not null %}
          <div class="panel panel-default">
            <div class="panel-heading">{{ calendario.1 }} <button type="button" class="btn btn-success btn-xs" onclick="$(location).attr('href','viaje.insertar.php?fecha=2015-{{ mes }}-{% if calendario.1 < 10 %}0{{ calendario.1 }}{% else %}{{ calendario.1 }}{% endif %}');">+</button></div>            
            <table class="table .table-condensed">
              <thead>
                <tr>
                  <th>Proyecto</th>
                  <th>Técnico</th>
                  <th>Hora</th>
                  <th>Motivo</th>
                  <th>Vehiculo</th>
                  <th>Acción</th>
                </tr>
              </thead>
              {% if fiscales[calendario.1] is not null %}
              <tbody>
                {% for fiscal in fiscales[calendario.1] %}
                <tr bgcolor="{{ fiscal.color }}">
                  {% set proyectos = fiscal.nombreProyecto|split(',') %}
                  <td>{% for proyecto in proyectos %}[{{ proyecto|convert_encoding('UTF-8', 'ISO-8859-1') }}]<br />{% endfor %}</td>
                  <td>{{ fiscal.alias }}</td>                  
                  <td>{{ fiscal.hora }}</td>
                  <td>{{ fiscal.motivo }}</td>   
                  <td>{{ fiscal.vehiculo }}</td>
                  <td><a href="viaje.eliminar.php?id={{ fiscal.id }}" class="btn btn-danger btn-xs popconfirm">-</a> <button type="button" class="btn btn-primary btn-xs" onclick="$(location).attr('href','viaje.editar.php?id={{ fiscal.id }}');">*</button></td>
                </tr>
                {% endfor %}
              </tbody>
              {% endif %}
            </table>
          </div>
        {% endif %}
        </td>
        <td>
        {% if calendario.2 is not null %}
          <div class="panel panel-default">
            <div class="panel-heading">{{ calendario.2 }} <button type="button" class="btn btn-success btn-xs" onclick="$(location).attr('href','viaje.insertar.php?fecha=2015-{{ mes }}-{% if calendario.2 < 10 %}0{{ calendario.2 }}{% else %}{{ calendario.2 }}{% endif %}');">+</button></div>            
            <table class="table .table-condensed">
              <thead>
                <tr>
                  <th>Proyecto</th>
                  <th>Técnico</th>
                  <th>Hora</th>
                  <th>Motivo</th>
                  <th>Vehiculo</th>
                  <th>Acción</th>
                </tr>
              </thead>
              {% if fiscales[calendario.2] is not null %}
              <tbody>
                {% for fiscal in fiscales[calendario.2] %}
                <tr bgcolor="{{ fiscal.color }}">
                  {% set proyectos = fiscal.nombreProyecto|split(',') %}
                  <td>{% for proyecto in proyectos %}[{{ proyecto|convert_encoding('UTF-8', 'ISO-8859-1') }}]<br />{% endfor %}</td>
                  <td>{{ fiscal.alias }}</td>                  
                  <td>{{ fiscal.hora }}</td>
                  <td>{{ fiscal.motivo }}</td>                  
                  <td>{{ fiscal.vehiculo }}</td>
                  <td><a href="viaje.eliminar.php?id={{ fiscal.id }}" class="btn btn-danger btn-xs popconfirm">-</a> <button type="button" class="btn btn-primary btn-xs" onclick="$(location).attr('href','viaje.editar.php?id={{ fiscal.id }}');">*</button></td>
                </tr>
                {% endfor %}
              </tbody>
              {% endif %}
            </table>
          </div>
        {% endif %}
        </td>
        <td>
        {% if calendario.3 is not null %}
          <div class="panel panel-default">
            <div class="panel-heading">{{ calendario.3 }} <button type="button" class="btn btn-success btn-xs" onclick="$(location).attr('href','viaje.insertar.php?fecha=2015-{{ mes }}-{% if calendario.3 < 10 %}0{{ calendario.3 }}{% else %}{{ calendario.3 }}{% endif %}');">+</button></div>            
            <table class="table .table-condensed">
              <thead>
                <tr>
                  <th>Proyecto</th>
                  <th>Técnico</th>
                  <th>Hora</th>
                  <th>Motivo</th>
                  <th>Vehiculo</th>
                  <th>Acción</th>
                </tr>
              </thead>
              {% if fiscales[calendario.3] is not null %}
              <tbody>
                {% for fiscal in fiscales[calendario.3] %}
                <tr bgcolor="{{ fiscal.color }}">
                  {% set proyectos = fiscal.nombreProyecto|split(',') %}
                  <td>{% for proyecto in proyectos %}[{{ proyecto|convert_encoding('UTF-8', 'ISO-8859-1') }}]<br />{% endfor %}</td>
                  <td>{{ fiscal.alias }}</td>                  
                  <td>{{ fiscal.hora }}</td>
                  <td>{{ fiscal.motivo }}</td>
                  <td>{{ fiscal.vehiculo }}</td>
                  <td><a href="viaje.eliminar.php?id={{ fiscal.id }}" class="btn btn-danger btn-xs popconfirm">-</a> <button type="button" class="btn btn-primary btn-xs" onclick="$(location).attr('href','viaje.editar.php?id={{ fiscal.id }}');">*</button></td>
                </tr>
                {% endfor %}
              </tbody>
              {% endif %}
            </table>
          </div>
        {% endif %}
        </td>
        <td>
        {% if calendario.4 is not null %}
          <div class="panel panel-default">
            <div class="panel-heading">{{ calendario.4 }} <button type="button" class="btn btn-success btn-xs" onclick="$(location).attr('href','viaje.insertar.php?fecha=2015-{{ mes }}-{% if calendario.4 < 10 %}0{{ calendario.4 }}{% else %}{{ calendario.4 }}{% endif %}');">+</button></div>            
            <table class="table">
              <thead>
                <tr>
                  <th>Proyecto</th>
                  <th>Técnico</th>
                  <th>Hora</th>
                  <th>Motivo</th>
                  <th>Vehiculo</th>
                  <th>Acción</th>
                </tr>
              </thead>
              {% if fiscales[calendario.4] is not null %}
              <tbody>
                {% for fiscal in fiscales[calendario.4] %}
                <tr bgcolor="{{ fiscal.color }}">
                  {% set proyectos = fiscal.nombreProyecto|split(',') %}
                  <td>{% for proyecto in proyectos %}[{{ proyecto|convert_encoding('UTF-8', 'ISO-8859-1') }}]<br />{% endfor %}</td>
                  <td>{{ fiscal.alias }}</td>                  
                  <td>{{ fiscal.hora }}</td>
                  <td>{{ fiscal.motivo }}</td>
                  <td>{{ fiscal.vehiculo }}</td>
                  <td><a href="viaje.eliminar.php?id={{ fiscal.id }}" class="btn btn-danger btn-xs popconfirm">-</a> <button type="button" class="btn btn-primary btn-xs" onclick="$(location).attr('href','viaje.editar.php?id={{ fiscal.id }}');">*</button></td>
                </tr>
                {% endfor %}
              </tbody>
              {% endif %}
            </table>
          </div>
        {% endif %}
        </td>
        <td>
        {% if calendario.5 is not null %}
          <div class="panel panel-default">
            <div class="panel-heading">{{ calendario.5 }} <button type="button" class="btn btn-success btn-xs" onclick="$(location).attr('href','viaje.insertar.php?fecha=2015-{{ mes }}-{% if calendario.5 < 10 %}0{{ calendario.5 }}{% else %}{{ calendario.5 }}{% endif %}');">+</button></div>            
            <table class="table .table-condensed">
              <thead>
                <tr>
                  <th>Proyecto</th>
                  <th>Técnico</th>
                  <th>Hora</th>
                  <th>Motivo</th>
                  <th>Vehiculo</th>
                  <th>Acción</th>
                </tr>
              </thead>
              {% if fiscales[calendario.5] is not null %}
              <tbody>
                {% for fiscal in fiscales[calendario.5] %}                
                <tr bgcolor="{{ fiscal.color }}">
                  {% set proyectos = fiscal.nombreProyecto|split(',') %}
                  <td>{% for proyecto in proyectos %}[{{ proyecto|convert_encoding('UTF-8', 'ISO-8859-1') }}]<br />{% endfor %}</td>
                  <td>{{ fiscal.alias }}</td>                  
                  <td>{{ fiscal.hora }}</td>
                  <td>{{ fiscal.motivo }}</td>
                  <td>{{ fiscal.vehiculo }}</td>
                  <td><a href="viaje.eliminar.php?id={{ fiscal.id }}" class="btn btn-danger btn-xs popconfirm">-</a> <button type="button" class="btn btn-primary btn-xs" onclick="$(location).attr('href','viaje.editar.php?id={{ fiscal.id }}');">*</button></td>>
                {% endfor %}
              </tbody>
              {% endif %}
            </table>
          </div>
        {% endif %}
        </td>
        <td>
        {% if calendario.6 is not null %}
          <div class="panel panel-default">
            <div class="panel-heading">{{ calendario.6 }} <button type="button" class="btn btn-success btn-xs" onclick="$(location).attr('href','viaje.insertar.php?fecha=2015-{{ mes }}-{% if calendario.6 < 10 %}0{{ calendario.6 }}{% else %}{{ calendario.6 }}{% endif %}');">+</button></div>            
            <table class="table .table-condensed">
              <thead>
                <tr>
                  <th>Proyecto</th>
                  <th>Técnico</th>
                  <th>Hora</th>
                  <th>Motivo</th>
                  <th>Vehiculo</th>
                  <th>Acción</th>
                </tr>
              </thead>
              {% if fiscales[calendario.6] is not null %}
              <tbody>
                {% for fiscal in fiscales[calendario.6] %}
                <tr bgcolor="{{ fiscal.color }}">
                  {% set proyectos = fiscal.nombreProyecto|split(',') %}
                  <td>{% for proyecto in proyectos %}[{{ proyecto|convert_encoding('UTF-8', 'ISO-8859-1') }}]<br />{% endfor %}</td>
                  <td>{{ fiscal.alias }}</td>                  
                  <td>{{ fiscal.hora }}</td>
                  <td>{{ fiscal.motivo }}</td>
                  <td>{{ fiscal.vehiculo }}</td>
                  <td><a href="viaje.eliminar.php?id={{ fiscal.id }}" class="btn btn-danger btn-xs popconfirm">-</a> <button type="button" class="btn btn-primary btn-xs" onclick="$(location).attr('href','viaje.editar.php?id={{ fiscal.id }}');">*</button></td>
                </tr>
                {% endfor %}
              </tbody>
              {% endif %}
            </table>
          </div>
        {% endif %}
        </td>
      </tr>
      {% set contador = contador + 1 %}
      {% endfor %}
    </tbody>
  </table>
</div>
