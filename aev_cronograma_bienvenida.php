<?php
/**
* @package AEV_Cronograma_Tabla
*/

$mesActual = date("m", time());

global $wpdb;
$resultado_viaje = $wpdb->get_results( "
SELECT cronograma.id, CONCAT(personal.nombre, ' ', personal.paterno, ' ', personal.materno) AS nombreCompleto, personal.cargo, personal.alias, personal.color,
cronograma.objetivoViaje, DATE_FORMAT(cronograma.fechaSalida, '%d/%m/%Y') AS fechaSalida, TIME_FORMAT(cronograma.horaSalida, '%H:%i') AS horaSalida, 
DATE_FORMAT(cronograma.fechaRetorno, '%d/%m/%Y') AS fechaRetorno, TIME_FORMAT(cronograma.horaRetorno, '%H:%i') AS horaRetorno, 
DATE_FORMAT(cronograma.fechaSalida, '%e') AS inicio, DATE_FORMAT(cronograma.fechaRetorno, '%e') AS final, vehiculo.nombreVehiculo, 
GROUP_CONCAT(proyecto.nombreProyecto) AS nombreProyecto, GROUP_CONCAT(proyecto.municipio) AS destino
FROM ((cronograma INNER JOIN personal ON cronograma.personalID = personal.id) INNER JOIN tipo ON personal.tipoID = tipo.id) INNER JOIN vehiculo ON cronograma.vehiculoID = vehiculo.id
LEFT JOIN cronogramaproyecto ON cronograma.id = cronogramaproyecto.cronogramaID
LEFT JOIN proyecto ON proyecto.id = cronogramaproyecto.proyectoID
WHERE ((MONTH(cronograma.fechaSalida) = '$mesActual') AND (MONTH(cronograma.fechaRetorno) = '$mesActual'))
GROUP BY cronograma.fechaSalida
" );
?>
<h1 align="center">Bienvenido al Sistema de Cronograma de Viajes</h1>

<div class="table-responsive">
  <table class="table table-bordered" border="1">
    <thead>
      <tr>
        <th rowspan="2">#</th>
        <th rowspan="2">Nombre y Apellidos</th>
        <th rowspan="2">Cargo</th>
        <th rowspan="2">Nombre del Proyecto</th>
        <th rowspan="2">Objetivo del Viaje</th>
        <th rowspan="2">Destino</th>
        <th colspan="2">Salida</th>
        <th colspan="2">Retorno</th>
        <th rowspan="2">Vehiculo</th>        
      </tr>
      <tr>      
        <th>Fecha</th>
        <th>Hora</th>
        <th>Fecha</th>
        <th>Hora</th>
      </tr>
    </thead>
    <tbody>      
      <?php
      	$contador = 1;
        foreach ($resultado_viaje as $viaje) {
      ?>      
      <tr>
        <td><?php print $contador; ?></td>
        <td><?php print $viaje->nombreCompleto; ?></td>
        <td><?php print $viaje->cargo; ?></td>
        <td><?php print $viaje->nombreProyecto; ?></td>
        <td><?php print $viaje->objetivoViaje; ?></td>
        <td><?php print $viaje->destino; ?></td>
        <td><?php print $viaje->fechaSalida; ?></td>
        <td><?php print $viaje->horaSalida; ?></td>
        <td><?php print $viaje->fechaRetorno; ?></td>
        <td><?php print $viaje->horaRetorno; ?></td>
        <td><?php print $viaje->nombreVehiculo; ?></td>
      </tr>
      <?php
      	$contador++;
        }
      ?>   
    </tbody>
  </table>
</div>