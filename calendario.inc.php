<?php
# Clase Estatica Calendario
class Calendario
{
	# Atributos públicos	

	# Constructor
	function __constructor()
	{

	}

	# Métodos

	# Método para calcular el numero del dia de la semana 0-6
	static function numeroDiaSemana($dia, $mes, $anio)
	{
		$numeroDiaSemana = date('w', mktime(0, 0, 0, $mes, $dia, $anio));
		if ($numeroDiaSemana == 0)
			$numeroDiaSemana = 6;
		else
			$numeroDiaSemana--;
		return $numeroDiaSemana;
	}

	# Método que devuelve el último dia de un mes y año dados
	static function ultimoDiaMes($mes, $anio)
	{
		$ultimoDia = 28;
		while (checkdate($mes, $ultimoDia, $anio))
		{
			$ultimoDia++;			
		}
		$ultimoDia--;
		return $ultimoDia;
	}

	# Método nombre del mes
	static function nombreMes($mes)
	{
		# Inicializar
		$nombre = NULL;
		switch ($mes) {
			case 1: $nombre = "Enero";  break;
			case 2: $nombre = "Febrero";  break;
			case 3: $nombre = "Marzo";  break;
			case 4: $nombre = "Abril";  break;
			case 5: $nombre = "Mayo";  break;
			case 6: $nombre = "Junio";  break;
			case 7: $nombre = "Julio";  break;
			case 8: $nombre = "Agosto";  break;
			case 9: $nombre = "Septiembre";  break;
			case 10: $nombre = "Octubre";  break;
			case 11: $nombre = "Noviembre";  break;
			case 12: $nombre = "Diciembre";  break;			
		}
		return $nombre;
	}

	
	# --------------------------------------
	static function mostrarMes($mes, $anio)
	{
		# Calculo el numero del día de la semana del primer día
		$numeroDia = self::numeroDiaSemana(1, $mes, $anio);

		# Calculo del último día del mes
		$ultimoDia = self::ultimoDiaMes($mes, $anio);

		#-----------------------------------------------------
		# Calculo para la primera fila, inicializamos variables
		$diaActual = 1;
		$calendarioMes = array();
		$numeroFila = 1;

		# Primera Fila
		$fila = array();
		for($i = 0; $i < 7; $i++)
		{
			if($i < $numeroDia)
			{
				$fila[$i] = NULL;
			}
			else
			{
				$fila[$i] = $diaActual;
				$diaActual++;
			}
		}

		# Una vez concluido eso asignamos al array que va a devolver el método
		$calendarioMes = array($numeroFila => $fila);

		# Seguimos para las demas filas
		# Inicializamos variables
		$numeroDia = 0;
		$numeroFila = 2;
		$fila = array();
		while($diaActual <= $ultimoDia)
		{
			#Comenzamos llenando la información
			$fila[] = $diaActual;
			$diaActual++;
			$numeroDia++;		

			# Ultimo dia de la semana
			if ($numeroDia == 7)
			{
				$numeroDia = 0;
				$calendarioMes[$numeroFila] = $fila;
				$numeroFila++;
				$fila = array();
			}	
		}

		# Caso de la ultima semana si la hay para completar
		if (sizeof($fila) > 0)
		{	
			for ($i = $numeroDia; $i < 7; $i++)
			{
				$fila[$i] = NULL;	
			}
			$calendarioMes[$numeroFila] = $fila;
		}

		return $calendarioMes;
	}

	# Misma que la anterior pero con datos de la base de datos
	static function mostrarMesBD($mes, $anio, $arreglo)
	{

	}
}

?>