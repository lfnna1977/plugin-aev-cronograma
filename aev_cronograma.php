<?php
/**
 * @package AEV_Cronograma
 */
/*
Plugin Name: Cronograma AEV
Plugin URI: http://www.sucreciudadblanca.com/pvv/
Description: Plugin para el llenado del cronograma de viajes.
Version: 0.0.1
Author: Luis Fernando Numa Navarro Arias
Author URI: http://www.sucreciudadblanca.com
License: GPLv2 or later
Text Domain: aev_cronograma
*/

function theme_styles() {

   wp_enqueue_style( 'bootstrap_css', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css' );
   wp_enqueue_style( 'main_css', get_stylesheet_directory_uri() . '/style.css' );

}

add_action( 'wp_enqueue_scripts', 'theme_styles');

function theme_js() {

  global $wp_scripts;

  wp_enqueue_script( 'bootstrap_js', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js');
  wp_enqueue_script( 'main_js', get_stylesheet_directory_uri() . '/javascript.js' );

}

add_action( 'wp_enqueue_scripts', 'theme_js' );

/*
* Añadiendo algunas funciones
*/
// Añadiendo un nuevo menu
add_action( 'admin_menu', 'aev_plugin_menu' );
// Añadiendo submenus al sistema
add_action( 'admin_menu', 'aev_add_sublevel_menu' );
function aev_plugin_menu(){
	add_menu_page('Opciones Cronograma de Viajes', 'Cronograma', 'manage_options', 'aev-cronograma', 'aev_pantalla_bienvenida');
}
function aev_add_sublevel_menu(){
	add_submenu_page(
		'aev-cronograma',
		'Insertar un viaje',
		'Insertar',
		'manage_options',
		'aev-insertar',
		'aev_formulario_cronograma_viaje'
	);
}


function aev_formulario_cronograma_viaje(){
	include ('aev_cronograma_insertar_viaje.php');
	include ('aev_cronograma_insertado_viaje.php');
}

function aev_pantalla_bienvenida(){
	include('aev_cronograma_bienvenida.php');
}
/*
* Para el frontend
*/

add_shortcode( 'aev_cronograma', 'aev_cronograma' );

function aev_cronograma(){
	include ('aev_cronograma_tabla.php');
}
?>