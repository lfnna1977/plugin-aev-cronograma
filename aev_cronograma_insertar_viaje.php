<?php
/**
* @package Formulario base para la introducción de información
*/

include('calendario.inc.php');

global $wpdb;
$resultado_persona = $wpdb->get_results( "
SELECT personal.id, CONCAT(personal.nombre, ' ', personal.paterno, ' ', personal.materno) AS nombreCompleto
FROM personal INNER JOIN tipo ON personal.tipoID = tipo.id
WHERE personal.habilitado = 1 
ORDER BY personal.nombre
" );
//print_r($resultado_persona);

$resultado_proyecto = $wpdb->get_results( "
SELECT id, nombreProyecto, nombreCorto 
FROM proyecto 
WHERE habilitado = 1
ORDER BY nombreProyecto
" );

$resultado_motivo = $wpdb->get_results( "
SELECT id, nombreMotivo 
FROM motivo 
WHERE habilitado = 1 
ORDER BY nombreMotivo
" );

$resultado_vehiculo = $wpdb->get_results( "
SELECT id, nombreVehiculo 
FROM vehiculo 
WHERE habilitado = 1 
ORDER BY nombreVehiculo
" );


$calendarioMes = Calendario::mostrarMes(date("n", strtotime(date('Y-m-d'))), date("Y", strtotime(date('Y-m-d'))));
	# Calculando de nuevo la posicion
	$buscarDia = date("j", strtotime(date('Y-m-d')));		
	foreach ($calendarioMes as $llaveCalendarioMes => $valorCalendarioMes) {
		# code...
		foreach ($valorCalendarioMes as $llave => $valor) {
			# code...
			if ($valor == $buscarDia)
				$posicionMes = $llaveCalendarioMes;
		}
	}
?>
<h3 align="center">Insertar Viaje</h3>

<form class="form-horizontal" method="post">

  <input type="hidden" name="posicionMes" value="<?php print $posicionMes; ?>">

  <div class="form-group">
    <label for="selectNombreCompleto" class="col-sm-2 control-label">Nombre y Apellido</label>
    <div class="col-sm-10">      
      <select class="form-control" id="selectNombreCompleto" name="selectNombreCompleto" required>        
      <?php      
      	foreach ($resultado_persona as $persona) {
      ?>
        <option value="<?php print $persona->id; ?>"><?php print $persona->nombreCompleto; ?></option>
      <?php
  		}  		  	  
      ?>
      </select>
    </div>
  </div>
  <div class="form-group">
    <label for="selectNombreProyecto" class="col-sm-2 control-label">Nombre del Proyecto</label>
    <div class="col-sm-10">
      <select class="form-control" id="selectNombreProyecto" name="selectNombreProyecto" required>
        <?php
        	foreach ($resultado_proyecto as $proyecto) {
        ?>
        <option value="<?php print $proyecto->id; ?>"><?php print $proyecto->nombreProyecto; ?></option>
        <?php
        	}
        ?>
      </select>      
    </div>
  </div>
  <div class="form-group">
    <label for="selectObjetivoCorto" class="col-sm-2 control-label">Objetivo del Viaje</label>
    <div class="col-sm-10">
      <select class="form-control" id="selectObjetivoCorto" name="selectObjetivoCorto" required>
        <?php
        	foreach ($resultado_motivo as $motivo) {
        ?>
        <option value="<?php print $motivo->id; ?>"><?php print $motivo->nombreMotivo; ?></option>
        <?php
        	}
        ?>
      </select>      
    </div>
  </div>  
  <div class="form-group">
    <label for="inputObjetivoViaje" class="col-sm-2 control-label">Descripción del Viaje</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" id="inputObjetivoViaje" name="inputObjetivoViaje" placeholder="Descripción completa del  Viaje" required>
    </div>
  </div>
  
  <div class="form-group">
    <label for="inputFechaSalida" class="col-sm-2 control-label">Fecha de Salida</label>
    <div class="col-sm-5"><input type="date" class="form-control" id="inputFechaSalida" name="inputFechaSalida" value="{{ fecha }}" required></div>
  </div>
  <div class="form-group">
    <label for="inputHoraSalida" class="col-sm-2 control-label">Hora de Salida</label>
    <div class="col-sm-5"><input type="time" class="form-control" id="inputHoraSalida" name="inputHoraSalida" required></div>
  </div>
  <div class="form-group">
    <label for="inputFechaRetorno" class="col-sm-2 control-label">Fecha de Retorno</label>
    <div class="col-sm-5"><input type="date" class="form-control" id="inputFechaRetorno" name="inputFechaRetorno" required></div>
  </div>
  <div class="form-group">
    <label for="inputHoraRetorno" class="col-sm-2 control-label">Hora de Retorno</label>
    <div class="col-sm-5"><input type="time" class="form-control" id="inputHoraRetorno" name="inputHoraRetorno" required></div>
  </div>
  <div class="form-group">
    <label for="selectVehiculo" class="col-sm-2 control-label">Vehiculo</label>
    <div class="col-sm-10">      
      <select class="form-control" id="selectVehiculo" name="selectVehiculo" required>
        <?php
        	foreach ($resultado_vehiculo as $vehiculo) {
        ?>
        <option value="<?php print $vehiculo->id; ?>"><?php print $vehiculo->nombreVehiculo; ?></option>        
        <?php
        	}
        ?>
      </select>
    </div>
  </div>
  <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
      <button type="submit" class="btn btn-default" name="enviar">Insertar Viaje</button>
    </div>
  </div>
</form>